package ShoppingSystem_Project;

import java.util.ArrayList;
import java.util.Scanner;

class Item{
	
	Item(String n , int c, double p, int q, String d){
		name = n;
		code = c;
		price = p;
		qty = q;
		description = d;
	}
	
	String name, description;
	int code, qty;
	double price;
	
	String getName(){
		return name;
	}
	
}


public class Main {	
	
	/*
	This is a shopping system prototype 

	The Requirements
	0.Descriptions of the items 
	 -name
	 -code
	 -quantity
	 -description
	 -discount/promotion/vouchers
	 
	1. List of products and cost
	2. Cart 
	3. payment system
	4. Search are browsing
	5. delivery options
	6. order confirmation

	---------------------------------

	Algorithm;

	Start
	0- sign up option
	1- login system
	 -enter user name and password
	 -check to make sure it is correct
	   *if correct let in
	   **else show message of invalid password
	2- List of products
	 -item code
	 -name
	 -price
	3- select a product
	 - ask user to enter item code
	 -enter item code
	 -ask for the quantity
	 -check is it is available and in stock
	   *if available add it to the cart
	   **else show message out of stock
	4- view cart with the total cost 
	 -expand with using a method

	*/
	
	// -------------------------------------- global variables ------------------------------------------ //
	
	// This is the database of the shop items with all the information needed inside
	//static ArrayList<String> itemName = new ArrayList<String>();
	//static ArrayList<Integer> itemCode = new ArrayList<Integer>();
	//static ArrayList<Integer> itemQty = new ArrayList<Integer>();
	//static ArrayList<Double> itemPrice = new ArrayList<Double>();
	//static ArrayList<String> itemDesc = new ArrayList<String>();
	
	// This is the data base for the cart of item that have being bought
	  //change the basket as well
	  
	//static ArrayList<String> basketName = new ArrayList<String>();
	//static ArrayList<Integer> basketCode = new ArrayList<Integer>();
	//static ArrayList<Integer> basketQty = new ArrayList<Integer>();
	//static ArrayList<Double> basketPrice = new ArrayList<Double>();
	//static ArrayList<String> basketDesc = new ArrayList<String>(); // This one not necessary 
	
	//creating a variable option
	//this will use the new class
	static ArrayList<Item> stocks = new ArrayList<Item>();
	
	static ArrayList<Item> basket = new ArrayList<Item>();
	
	static Scanner inp = new Scanner(System.in);
	
	// ------------------------------------------------------------------------------------------------- //

	public static void main(String[] args) {
		
		// add some items into the store, ( for example shirts, pens and books )
		// do all this before the start of the shopping code.
		
		stocks.add(new Item("Shirt", 1, 9.99, 15, "a nice shirt"));
		
		stocks.add(new Item("Pen", 2, 2.99, 56, "A pen fool"));
		
		stocks.add(new Item("Shoes", 3, 12.99, 34, "sandles"));
		
		stocks.add(new Item("Book", 4, 4.99, 57, "A book"));
		
		//itemName.add("Book");
		//itemCode.add(3);
		//itemPrice.add(4.99);
		//itemQty.add(12);
		//itemDesc.add("a book on many things");
		
		// Now it is time for the sign up system ( will start by asking a user to enter a
		// or password
		
		// --------------------------------------- local variables ------------------------------------------ //
		
		String username = "username", password = "123";
		int selectionCode, selectionQty, yesOrNo;
		String mangusername = "mang", mangpassword = "password";
		String addrLine1, addrLine2, city, country, postCode;
		
		// -------------------------------------------------------------------------------------------------- //
		
		
		/* - sign in ---------------------------------------------------------------------------------------- */
		
		// create a sign up system for the shopping (method)
		
		while(true) {
		System.out.println("please enter you username and then password" );
		// asking for the user name input
		String usernameinp = inp.nextLine();
		// and password
		String passwordinp = inp.nextLine();
		
		//will make sure that the password isn't that of the administrator
		if((mangusername == usernameinp) && (mangpassword == password)) {
			System.out.println("you are entering the admin area ...");
			adminMenu();			
		}
		
		
		// Now i will check to make sure that the user name matches the correct user name and password
		if((username == usernameinp) && (password == passwordinp)) {
			System.out.println("This Username and Passord is correct");
			break;
		}else {
			System.out.println("This username and password is incorrect");
		}
		}//end of while
		
		/* -------------------------------------------------------------------------------------------------- */
		
		/* - shopping --------------------------------------------------------------------------------------- */
		
		do {
		// now the user has passed the account check we will list the products (will put into method later)
		itemMenuDisplay();
		
		// selecting a product 
		System.out.println("please enter the code for the item that you wish to buy, or "
				+"0 for payment");
		
		selectionCode = inp.nextInt();
		if(selectionCode == 0) { // remember that selection code has to be 1 unit lower because arrays start at 0 not 1
			//payment method
			paymentMethod();
			break;
			
		}else {
			System.out.println("how many of them would you like?");
			selectionQty = inp.nextInt();
			
			
			//if(stocks.get((selectionCode - 1)) >= selectionQty)
			if(stocks.get(selectionCode).qty >= selectionQty) {
				//adding item to cart if they are in stock
				
				basketCode.add(selectionCode);
				basketQty.add(selectionQty);
				basketName.add(stocks.get(selectionCode - 1).name;
				basketPrice.add((stocks.get(selectionCode - 1).price * (basket.get(selectionCode - 1).qty; 	
			}else {
				System.out.println("I am sorry but that item is out of stock their are only "
			                 +stocks.get(selectionCode -1).qty + " feft in stock");
			}
		}
		System.out.println("This is your basket ...");
		
		displayBasket();
		
		
		} while(true);//end of do
		
		/* -------------------------------------------------------------------------------------------------- */
		
		/* - delivery options ------------------------------------------------------------------------------- */
		
		System.out.println("please enter your address that you would like your items to be deilvered to ...");
		
		// askForAddress(addrLine1, addrLine2, city, country, postCode);
		
		
		/* -------------------------------------------------------------------------------------------------- */
		
		/* - confirmations ---------------------------------------------------------------------------------- */
		
		
		
		/* -------------------------------------------------------------------------------------------------- */
		
	}// Ending of the main();

	
	//This will ask and record the address of the customer
	private static void askForAddress(String addrLine1, String addrLine2, String city, String country, 
			String postCode) {
		
		String option;
		
		do {
		System.out.println("What is your first line of your address");
		addrLine1 = inp.nextLine();
		System.out.println("What is your second line of your address");
		addrLine2 = inp.nextLine();
		System.out.println("What is your City of your address");
		city = inp.nextLine();
		System.out.println("What is your Country of your address");
		country = inp.nextLine();
		System.out.println("What is your Postcode");
		postCode = inp.nextLine();
		
		System.out.println("Is this correct if no type n if yes type y");
		System.out.println(addrLine1);
		System.out.println(addrLine2);
		System.out.println(city);
		System.out.println(country);
		System.out.println(postCode);
		
		option = inp.next();
		
		}while(option != "n");
		
		
	}//end of askForAddress

	//method that brings up all the options of the administrators
	private static void adminMenu() {
		
		int option;
		
		do {
		System.out.println("What function would you like to do 1 for add item 2 for remove item 3 "
				+"add voucher or coupon 0 to exit menu ...");
		
	    option = inp.nextInt();
		switch(option) {
		 case 1:
			 mangAddItem();
			 break;
		 case 2:
			 mangRemoveItem();
			 break;
		 case 3:
			 mangAddVoucherOrCoupon();
			 break;
		 case 0:
			 break;
			 
			 default:
				 System.out.println("this is not a vaild option");
		}
		}while(option != 0);
	}// end of adminMenu();

	//method for adding a voucher or coupon
	private static void mangAddVoucherOrCoupon() {
		
		
	}//end of mangAddVoucherOrCoupon();

	//method for removing items form the menu
	private static void mangRemoveItem() {
		
		
	}//end of mangRemoveItem();

	//method for adding item to array
	private static void mangAddItem() {
		String tempName, tempDesc;
		double tempPrice;
		int tempCode, tempQty, option = 1;
		
		while(option != 0) {
		
		System.out.println("What is the name of the item you wish to add");
		tempName = inp.nextLine();
		
		System.out.println("What is the code of the item you wish to add");
		tempCode = inp.nextInt();
		
		System.out.println("What is the Qty of the item you wish to add");
		tempQty = inp.nextInt();
		
		System.out.println("What is the Price of the item you wish to add");
		tempPrice = inp.nextDouble();
		
		System.out.println("What is the Description of the item you wish to add");
		tempDesc = inp.nextLine();
		
		itemName.add(tempName);
		itemCode.add(tempCode);
		itemPrice.add(tempPrice);
		itemQty.add(tempQty);
		itemDesc.add(tempDesc);
		
		System.out.println("Would you like to add more items 1 for yes and 0 for no ...");
		option = inp.nextInt();
		
		}
		
	}//end of mangAddItem();

	//method for displaying the basket of items that you have put in your cart ...
	private static void displayBasket() {
		for ( int i = 0; i < basketName.size(); i++) {
			System.out.print("Name - " + basketName.get(i) + "\t Code - " + basketCode.get(i) + "\t Qty - " 
		      + basketQty.get(i) + "\t Price - " + basketPrice.get(i));
		}
		
	}// end of displayBasket();

	//Method that displays the menu of the items in the shop ...
	private static void itemMenuDisplay() {
		for(Item i : stocks ) {
			System.out.print("Name - " + i.name + "\t Code - " + i.code + "\t Qty - " 
					+ i.qty + "\t Price - " + i.price + "\t Description - " + i.description);
		}
		
		//for ( int i = 0; i < itemName.size(); i++) {
		//	System.out.print("Name - " + itemName.get(i) + "\t Code - " + itemCode.get(i) + "\t Qty - " 
		//      + itemQty.get(i) + "\t Price - " + itemPrice.get(i) + "\t Description - " + itemDesc.get(i));
		//}
		
		
	}// end of itemMenuDisplay();

	//Method that will process the payment and give change ...
	private static void paymentMethod() {
		displayBasket();
		
		float cost = 0;
		for(int i=0; i< basketName.size(); i++ ) {
			cost += basketPrice.get(i);
		}
		
		System.out.println("your total cost is " + cost);
		
		
		while(true) {
		System.out.println("enter an amount for payment");
		float payment = inp.nextFloat();
		
		if (payment >= cost ) {
			// gives change
			float change = payment - cost;
			System.out.println("Thank you for your payment your have " + change + " change");
			break;
		}else {
			System.out.println("This is not enought money to cover the cost");
		}
		
		}//end of while
		
		
		
	}// end of paymentMethod();

}









//