import java.util.*;

/* TODO restaurant booking system ....
 
 -create an array for the tables
 	-list the tables
 -welcome and list the tables on the screen
 	(method)showing the tables
 -ask the customer their name and which table they would like to sit in
 	-add info to Accounts(Class) .. constructor
 	-if seats are not available marked with XX 
 		- sent msg "seats are not available select another" (method)
 		-once a seat has being selected add the selection to Accounts(Class) and mark with XX on array
 -then display a menu as seen in the assignment sheet Food(Class) should be created with 
 arraylist of all the food with price and other info on	(codes, quantities and prices)
 	-add methods in class for adding and removing items from basket Basket(Class)
 -switch statment for adding removing and exiting shop for the check out / payment
 -once exit payment method which will add up total cost and charge the customer
 -conformation of everything (name seatnumber , list of order items and total cost )
 		

*/
public class main {

	static Scanner inp = new Scanner(System.in);
	static String[] tables = {"[01]", "[XX]", "[03]", "[04]", "[05]", "[06]", "[07]", "[08]", "[09]"
			, "[10]", "[11]", "[12]", "[13]", "[14]", "[15]", "[16]", "[17]", "[18]", "[19]"
			, "[20]", "[21]", "[22]", "[23]", "[24]", "[25]", "[26]", "[27]", "[28]", "[29]"
			, "[30]", "[31]", "[32]", "[33]", "[34]", "[35]", "[36]", "[37]", "[38]", "[39]"
			, "[40]", "[41]", "[42]"};
	
	static ArrayList<Food> food = new ArrayList<Food>();
	static ArrayList<Food> basket = new ArrayList<Food>();
	
	public static void main(String[] args) {
		
		// variables ...
		int tempSel, selection = 0;
		String tempName;
		
		food.add(new Food(1, "Pizza     ", 10, 50));
		food.add(new Food(2, "Steak     ", 7, 50));
		food.add(new Food(3, "Sandwich  ", 5, 50));
		food.add(new Food(4, "Water     ", 1, 30));
		food.add(new Food(5, "Soft drink", 2, 30));
		food.add(new Food(6, "tea       ", 2, 20));
		food.add(new Food(7, "Coffee    ", 2, 20));
		food.add(new Food(8, "Ice Cream ", 2, 20));
		food.add(new Food(9, "Chocolate ", 2, 20));

		
		System.out.println("---Welcome to the booking system---");
		displayTables();

		
		System.out.println("What is you name? ....");
		tempName = inp.nextLine();
		
		while(true) {
		System.out.println("-Which table would you like to sit in ...");
		tempSel = inp.nextInt();
		
		if(tables[tempSel - 1] == "[XX]") {
			System.out.println("I am sorry this table it not avaliable at the moment please choose"
					+"again");
		}else {
			System.out.println("You will be sitting at table" + tempSel);
			//changing number to XX to mark taken
			tables[tempSel -1] = "[XX]";
			displayTables();
			break;
		}
		}//end of while loop
		
		//loop for shopping for food ..
		do {
		listFoods();
		
		System.out.println("---What would you like to do---"
				+"\n1 - add item to basket"
				+"\n2 - remove item from basket"
				+"\n3 - Exit to check out");
		
		selection = inp.nextInt();

		switch(selection) {
		case 1:
			addToBasket();
			break;
		case 2:
			removeForBasket();
			break;
		case 3:
			break;
			default:
				System.out.println("sorry this is not a valid option please pick from the list above");
	
		}
		}while(selection != 3);//end of while
		
		System.out.println("This is the basket ..");
		displayBasket();
		
		double totalCost = 0;
		for(int i = 0; i < basket.size(); i ++) {
			totalCost = totalCost + basket.get(i).price;
		}
			
		paymentDisplay(totalCost);
		
		System.out.println("Thanks you for your purchase " + tempName
				+ "\nSeat number - " + (tempSel -1)
				+ "\nTotal Cost - " + totalCost);
		
		displayBasket();
		
		
	}//end of main();

	//method for payment at the end of program
	private static void paymentDisplay(double totalCost) {
		double amountPayed;
		
		while(true) {
			
		System.out.println("Your total cost is " + totalCost);
		System.out.println("Enter the amount you will be paying");
		amountPayed = inp.nextDouble();
		
		if(amountPayed >= totalCost) {
			System.out.println("Thank you for your payment your change is .." + (amountPayed - totalCost));
			break;
		}else {
			System.out.println("You do not have enough money to pay for this ...");
		}
		}//end while
		
	}//end of paymentDisplay();

	//method for displaying the basket after all purchases ...
	// TODO fix this display as well ..
		//@price the overall price of item price * quantity
		//@quantity the amount of the item that you are buying
	private static void displayBasket() {

		System.out.println("----------Basket---------");
		System.out.println("Code\t item\t\t Price\t Quantity");
		
		for(int i = 0; 0 < basket.size(); i++) {
			System.out.println(basket.get(i).code +"\t " + basket.get(i).name + "\t �" + basket.get(i).price
					+ "\t " + basket.get(1).quanity);
		}
		
	}//end of displayBasket();

	private static void removeForBasket() {
		
		displayBasket();
		int tempCode;
		String tempOption;
		
		do {
			System.out.println("Enter the code of the items you wish to remove ... ");
			tempCode = inp.nextInt();
			
			food.remove(tempCode - 1);
			
			System.out.println("If you would like to remove another item enter yes if you would like to"
					+ "leave back into the menu type no");
			tempOption = inp.nextLine().toLowerCase();
		}while(tempOption == "yes");

		
	}//end of removeFromBasket();

	private static void addToBasket() {
		
		String tempOption;
		
		do {
		System.out.println("What is the code of the item that you wish to purchase ...");
		int tempCode = inp.nextInt();
		
		System.out.println("How many would you like");
		int tempQuantity = inp.nextInt();
		if(tempQuantity > food.get(tempCode -1).quanity) {
			System.out.print("You have added " + tempQuantity + " " + food.get(tempCode -1).name);
			//adding the selection to the basket array  
			basket.add(new Food(food.size()+1, (food.get(tempCode - 1).name), 
					(food.get(tempCode - 1).price*tempQuantity), tempQuantity ));
		}else {
			System.out.println("Sorry their are not enough of that item in stock");
			
		}
		
		System.out.println("If you would like to add another item enter yes if you woudl like to"
				+ "leave back into the menu type no");
		
		tempOption = inp.nextLine().toLowerCase();
		}while(tempOption == "yes");
		
	}//end of addToBasket();

	//method for listing the food available ...
	// TODO fix how the items are displayed
	private static void listFoods() {
		
		System.out.println("----------Menu---------");
		System.out.println("Code\t item\t\t Price\t Quantity");
		
		for(int i = 0; 0 < food.size(); i++) {
			System.out.println(food.get(i).code +"\t " + food.get(i).name + "\t �" + food.get(i).price
					+ "\t " + food.get(1).quanity);
		}
	}

	//method for displaying the tables menu ...
	private static void displayTables() {
		
		for(int i = 0; i<tables.length;i++) {
			for(int j = 0; j<6; j++) {
				System.out.print(tables[i]+ " ");
				i++;
			}
			System.out.println("");
		}
		
		
	}//end of displayTables();

}//end of mainClass


class Food{
	
	int code, quanity;
	String name;
	double price;
	
	Food(int c, String n, double p, int q){
		code = c;
		name = n;
		price = p;
		quanity = q;
	}	
}//end of FoodClass();




