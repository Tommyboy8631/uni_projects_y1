import java.util.*;

class Accounts{
	
	Accounts(String n, int c, int tempAccountNumber){
		
		name = n;
		code = c;
		accountNumber = tempAccountNumber;
		balance = 0;
	}
	String name;
	int code, accountNumber;
	double balance;
	
}

public class main {
	
	static ArrayList<Accounts> acc = new ArrayList<Accounts>();
	static Scanner inp = new Scanner(System.in);

	public static void main(String[] args) {
		
		//adding a few Accounts to get started
		acc.add(new Accounts("Tom", 0, 174946));
		acc.add(new Accounts("paul", 1, 175834));

		
		while(true) {
		System.out.println("-------Menu-------"
				+"\n1 - sign-in"
				+"\n2 - create an account");
		
		int option = inp.nextInt();
		if(option == 2) {
			signUp();
		}if(option == 1){
			break;
		}else {
			System.out.println("this is not a vaild answer ..."
					+"enter from the menu above");
		}	
		}
		
		System.out.println("what is your account code ...");
		int signInCode = inp.nextInt();
		
		int selectionOption;
		
		do {
		
		System.out.println("-----Selection Menu-----"
				+"\n1 - Depostit"
				+"\n2 - Withdraw"
				+"\n3 - Display Balance"
				+"\n4 - Exit");
		
		selectionOption = inp.nextInt();
		
		switch(selectionOption) {
		case 1:
			accountDeposit(signInCode);
			break;
		case 2:
			accountWithdraw(signInCode);
			break;
		case 3:
			accountDisplay(signInCode);
			break;
		case 4:
			break;
			default:                
				System.out.println("Please enter a vaild option ...");
		
		}
		}while(selectionOption == 4);
		
		
	}

	private static void accountDisplay(int s) {
		int accountCode = s;
		System.out.println("Your account code is ..." + acc.get(accountCode).balance);
		
	}//end of accountDisplay();

	private static void accountWithdraw(int s) {
		int accountCode = s;
		double option;
		
		while(true) {
		System.out.println("how much should you like to withdraw ...");
		option = inp.nextDouble();
		
		if(option > acc.get(accountCode).balance) {
			System.out.println("not enougth funds please enter a differnet value ...");
		}else {
			break;
		}
		}
		
		acc.get(accountCode).balance -= option;
		
	}//end of accountWithdraw();

	private static void accountDeposit(int s) {
		int accountCode = s;
		System.out.println("How mcuh would you like to deposit");
		double option = inp.nextDouble();
		
		acc.get(accountCode).balance += option;
		
	}//end of accountDeposit();

	//method for signing up ...
	private static void signUp() {
		
		int tempCode = acc.size() + 1;
		int tempAccountNumber;
		String tempName;
		
		System.out.println("What is the name you wish to use on your account ...");
		tempName = inp.next();
		
		tempAccountNumber = (int) (100000 + Math.random() * 999999);
		
		System.out.println("Your card info ..."
				+"Name - " + tempName
				+"Code - " + tempCode
				+"Account Number - " + tempAccountNumber);
		
		acc.add(new Accounts(tempName, tempCode, tempAccountNumber));
		
	}//end of signUp();

}
